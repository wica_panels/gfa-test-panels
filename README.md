# How to work with this GitLab Project

This project hosts three __completely independent__ branches named 'dist_dev', 'dist_prod' and 'dist_ext'.

They are intended for development, internal and external production purposes.

The main branch is not deployed anywhere. You can use it to hold a reference copy of your files which can then be copied manually to the respective branch where the software is to be deployed.

# Working on the Wica Development Server

The Wica Development Server is available internally within PSI at the following URL
https://gfa-wica-dev.psi.ch

The files in the 'dist_dev' branch can be deployed there using the following command:
```
curl -k -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://gfa-wica-dev.psi.ch:8443/deploy?gitUrl=git@gitlab.psi.ch:wica_panels/gfa-test-panels.git"
```

Alternatively you can deploy the software manually using the autodeployer service page at the following URL:
https://gfa-wica-dev.psi.ch:8443/service-configuration/html

# Working on the Wica Production Server

The Wica Production Server is available internally within PSI at the following URL
https://gfa-wica.psi.ch

The files in the 'dist_prod' branch can be deployed there using the following command:
```
curl -k -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://gfa-wica.psi.ch:8443/deploy?gitUrl=git@gitlab.psi.ch:wica_panels/gfa-test-panels.git"
```

Alternatively you can deploy the software manually using the autodeployer service page at the following URL:
https://gfa-wica.psi.ch:8443/service-configuration/html

# Working on the Wica External Server

The Wica External Server is available externally form outside PSI at the following URL:
https://wica.psi.ch

The files in the 'dist_ext' branch can be deployed there using the following command:
```
curl -k -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://wica.psi.ch:8443/deploy?gitUrl=git@gitlab.psi.ch:wica_panels/gfa-test-panels.git"
```

Alternatively you can deploy the software manually using the autodeployer service page at the following URL:
https://wica.psi.ch:8443/service-configuration/html
